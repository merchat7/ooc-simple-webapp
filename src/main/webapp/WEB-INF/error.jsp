<!DOCTYPE html>

<html lang="en">

<head>
    <title>Error page which redirects after two seconds</title>
    <meta http-equiv="refresh" content="2; URL=/">
    <meta name="keywords" content="automatic redirection">
</head>

<body>
<h2>An error occured...Redirecting...</h2>
</body>

</html>