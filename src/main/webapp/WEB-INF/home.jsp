<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html lang="en">

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>

<script>
    $(document).ready(function () {
        var modal = $('#errorModal');
        <c:if test="${not empty error_message}">
            modal.modal('show');
        </c:if>

        $('#userTable > tbody').on('click', '>tr', function () {
            $('#modifyUserModal').modal('show');
            var currentUser = '<%= request.getUserPrincipal().getName() %>';
            var tds = $(this).find('td');
            var username = tds.eq(0).text();
            document.getElementById("username").value = tds.eq(0).text();
            document.getElementById("firstName").value = tds.eq(2).text();
            document.getElementById("lastName").value = tds.eq(3).text();
            document.getElementById("origUsername").value = tds.eq(0).text();
            if (username === currentUser) {
                document.getElementById("modifyButton").style.display = "none";
                document.getElementById("removeButton").style.display = "none";
                $('#modifyForm :input').attr("readOnly", true);
            } else {
                document.getElementById("modifyButton").style.display = "block";
                document.getElementById("removeButton").style.display = "block";
                $('#modifyForm :input').attr('readOnly', false);
            }
        });

    });


</script>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="/profile"><span class="glyphicon glyphicon-user"></span> Profile</a></li>
            <li><a href="/logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <h2>Users</h2>
    <table class="table table-hover table-bordered" id="userTable">
        <thead>
        <tr>
            <th >Username</th>
            <th>Password</th>
            <th>First name</th>
            <th>Last name</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="user" items="${userList}">
            <tr>
                <td>${user.username}</td>
                <td>${user.password}</td>
                <td>${user.firstName}</td>
                <td>${user.lastName}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

<div class="container">
    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addUserModal">Add user</button>

    <!-- Modal -->
    <div class="modal fade" id="addUserModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add user</h4>
                </div>
                <form action="/adduser" method="post">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="FirstnameField">First name</label>
                                <input type="text" class="form-control input-sm" name = "firstname" id="FirstnameField" placeholder="First name">
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="LastnameField">Last name</label>
                                <input type="text" class="form-control input-sm" name="lastname" id="LastnameField" placeholder="Last name">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="UsernameField">Username</label>
                                <input type="text" class="form-control input-sm" name = "username" id="UsernameField" placeholder="Username">
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="PasswordField">Password</label>
                                <input type="text" class="form-control input-sm" name="password" id="PasswordField" placeholder="Password">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-4 col-sm-4 col-md-4" style="float: none; margin: 0 auto;">
                            <button type="submit" class="btn btn-primary btn-block" >Save</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="modifyUserModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modify User</h4>
                </div>
                <form action="/modifyuser" method="post" id="modifyForm" onSubmit="if(!confirm('Are you sure?')){return false;}">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="firstname">First name</label>
                                    <input type="text" class="form-control input-sm" name = "firstname" id="firstName" placeholder="First name">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="lastname">Last name</label>
                                    <input type="text" class="form-control input-sm" name="lastname" id="lastName" placeholder="Last name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input type="text" class="form-control input-sm" name = "username" id="username" placeholder="Username">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="text" class="form-control input-sm" name="password" id="password" placeholder="Password">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input class="hidden" name="origusername" id="origUsername">
                        <div class="row">
                            <div class="col-xs-4 col-sm-4 col-md-4" style="float: none; margin: 0 auto;">
                                <button type="submit" class="btn btn-primary btn-block" id="modifyButton">Save</button>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4" style="float: none; margin: 0 auto;">
                                <button type="submit" class="btn btn-danger btn-block" id="removeButton" name="removeButton" value ="remove" >Remove</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="errorModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Error</h4>
                </div>
                <div class="modal-body">
                    <p>${error_message}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
