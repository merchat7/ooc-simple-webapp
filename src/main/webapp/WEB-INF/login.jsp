<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="<%=request.getContextPath()%>resources/js/bootstrap.js"></script>

<html lang="en">

<head>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>resources/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>resources/css/login.css">
</head>

<body>

<div class ="container">
    <div class="col-md-4 col-sm-4 col-xs-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Sign In</h3>
            </div>
            <div class="panel-body">
                <c:if test="${not empty error_login}">
                    <div class="alert alert-danger">
                            ${error_login}
                    </div>
                </c:if>
                <form action="#" th:action="@{/login}" method="post">
                    <div class="form-group">
                        <label for="UsernameField">Username</label>
                        <input type="text" class="form-control" name = "username" id="UsernameField" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <label for="PasswordField">Password</label>
                        <input type="password" class="form-control" name="password" id="PasswordField" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

</body>

</html>
