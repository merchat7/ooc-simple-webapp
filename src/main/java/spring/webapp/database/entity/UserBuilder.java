package spring.webapp.database.entity;

public class UserBuilder {
    private String username;
    private String password;
    private String firstName;
    private String lastName;

    public UserBuilder setUsername (String username) {
        this.username = username;
        return this;
    }

    public UserBuilder setPassword (String password) {
        this.password = password;
        return this;
    }

    public UserBuilder setFirstName (String firstName) {
        this.firstName = firstName;
        return this;
    }

    public UserBuilder setLastName (String lastName) {
        this.lastName = lastName;
        return this;
    }

    public User build () {
        return new User(username, password, firstName, lastName);
    }
}
