package spring.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import spring.webapp.database.entity.User;
import spring.webapp.database.repository.UserRepository;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;

@Controller
public class UserController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping("/adduser")
    String addUser(Model model,
                   @RequestParam(value = "firstname", required=false) String firstName,
                   @RequestParam(value = "lastname", required=false) String lastName,
                   @RequestParam(value = "username", required=false) String username,
                   @RequestParam(value = "password", required=false) String password) {
        if (!username.isEmpty()) {
            String hashedPassword = passwordEncoder.encode(password);
            userRepository.save(new User(username, hashedPassword, firstName, lastName));
            return "redirect:/";
        }
        else {
            model.addAttribute("error_message", "Username is required");
            return "home";
        }
    }

    @PostMapping("/modifyuser")
    String modifyuser(Model model, HttpServletRequest request,
                      @RequestParam(value = "firstname", required=false) String firstName,
                      @RequestParam(value = "lastname", required=false) String lastName,
                      @RequestParam(value = "username", required=false) String username,
                      @RequestParam(value = "password", required=false) String password,
                      @RequestParam(value = "origusername", required=false) String origUsername) {
        String buttonPressed = request.getParameter("removeButton");
        if (buttonPressed != null && buttonPressed.equals("remove")) {
            userRepository.deleteUserByUsername(origUsername);
            return "redirect:/";
        }
        if (!username.isEmpty()) {
            User newUser = userRepository.findOneByUsername(username);
            if (newUser == null || username.equals(origUsername)) {
                User oldUser = userRepository.findOneByUsername(origUsername);
                String hashedPassword = passwordEncoder.encode(password);
                oldUser.setUsername(username);
                oldUser.setFirstName(firstName);
                oldUser.setLastName(lastName);
                oldUser.setPassword(hashedPassword);
                userRepository.save(oldUser);
                return "redirect:/";
            }
            else throw new DataIntegrityViolationException("");
        }
        else {
            model.addAttribute("error_message", "Username is required");
            return "home";
        }
    }

    @ExceptionHandler({SQLException.class,DataIntegrityViolationException.class})
    private String duplicateUsername(Model model) {
        model.addAttribute("error_message", "User name is already in used");
        return "home";
    }
}
