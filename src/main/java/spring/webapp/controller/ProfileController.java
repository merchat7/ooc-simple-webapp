package spring.webapp.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import spring.webapp.database.entity.User;
import spring.webapp.database.repository.UserRepository;


@Controller
public class ProfileController {
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/profile")
    public String profile(Model model, Authentication authentication) {
        String username = authentication.getName();
        User user = userRepository.findOneByUsername(username);
        model.addAttribute("username", user.getUsername());
        model.addAttribute("password", user.getPassword()); // is not currently shown
        model.addAttribute("firstName", user.getFirstName());
        model.addAttribute("lastName", user.getLastName());
        return "profile";
    }
}