package spring.webapp.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class LoginController {
    @GetMapping("/login")
    public String login(Model model, Authentication authentication, HttpServletRequest request) {
        if (authentication != null) return "redirect:/";

        Object errorLogin = request.getSession().getAttribute("error_login");
        if (errorLogin != null) {
            model.addAttribute("error_login", errorLogin); // to not display an error message on refresh
            request.getSession().removeAttribute("error_login");
        }
        return "login";
    }
}