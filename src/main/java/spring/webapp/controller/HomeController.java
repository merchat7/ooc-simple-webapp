package spring.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import spring.webapp.database.entity.User;
import spring.webapp.database.repository.UserRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class HomeController {
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/")
    public String home(Authentication authentication, HttpServletRequest request) {
        if (authentication == null) return "redirect:/login";
        List<User> userList = userRepository.findAll();
        request.getSession().setAttribute("userList", userList);
        return "home";
    }
}
